from django.shortcuts import render, redirect
from .models import URL
import random
import string

PORT = 8000

def home(request):
    if request.method == 'POST':
        original_url = request.POST.get('url')
        resource = request.POST.get('short')
        if not resource:
            resource = str(URL.objects.filter(resource__exact='').count() + 1)
            #resource = str(URL.objects.count() + 1)
        short_url, original_url = generate_short_url(original_url, resource)
        if short_url:
            URL.objects.create(original_url=original_url, short_url=short_url, resource=resource)
    urls = URL.objects.all()
    return render(request, 'home.html', {'urls': urls})


def redirect_original(request, resource):
    try:
        url = URL.objects.get(resource=resource)
        return redirect(url.original_url)
    except URL.DoesNotExist:
        return redirect('home')


def check_existing_url(original_url):
    try:
        url = URL.objects.get(original_url=original_url)
        return True
    except URL.DoesNotExist:
        return False


def generate_short_url(original_url, resource):
    if not (original_url.startswith("http://") or original_url.startswith("https://")):
        original_url = "https://" + original_url

    if not check_existing_url(original_url):
        short_url = f"http://localhost:{PORT}/acorta/{resource}"
        while URL.objects.filter(short_url=short_url).exists():
            resource = str(int(resource) + 1)
            short_url = f"http://localhost:{PORT}/acorta/{resource}"
        return short_url, original_url
