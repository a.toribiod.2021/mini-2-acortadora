from django.db import models


class URL(models.Model):
    original_url = models.CharField(max_length=100, default="")
    short_url = models.CharField(max_length=20, unique=True)
    resource = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.original_url
