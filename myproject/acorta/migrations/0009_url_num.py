# Generated by Django 5.0.3 on 2024-04-02 10:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("acorta", "0008_url_resource"),
    ]

    operations = [
        migrations.AddField(
            model_name="url",
            name="num",
            field=models.IntegerField(default=0),
        ),
    ]
